package org.landal.spoj

import org.scalatest.junit.ShouldMatchersForJUnit
import org.scalatest.junit.AssertionsForJUnit
import org.junit.Test

class PrimeTest extends AssertionsForJUnit with ShouldMatchersForJUnit {
  
  @Test def test_prime() {
    (Main.isPrime(55)) should equal (false)
  }

}