package org.landal.spoj

object Main {

  def main(args: Array[String]): Unit = {
    val numTests = Console.readInt
    Console.println(numTests)

    for (i <- 1 to numTests) {    
      val startIndex = Console.readInt
      val lastIndex = Console.readInt
      for (j <- startIndex to lastIndex if isPrime(j)) {
        Console.println(j)
      }
    }

  }

  def isPrime(n: Int): Boolean = {

    def isPrimeIter(i: Int, n: Int): Boolean = {
      if (n % i == 0) false
      else if (i * i < n) isPrimeIter(i + 2, n)
      else true
    }

    if (n % 2 == 0) false
    else isPrimeIter(3, n)
  }

}