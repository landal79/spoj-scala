package org.landal.spoj

import org.junit.Test
import org.scalatest.junit.AssertionsForJUnit
import org.scalatest.junit.ShouldMatchersForJUnit

class LifeTest extends AssertionsForJUnit with ShouldMatchersForJUnit {

  @Test def test_filter() {
    Life.filter(Array("12", "44", "56", "78", "42", "11", "67")) should equal(Array("12", "44", "56", "78"))
  }

  @Test def test_filter_fail() {
    Life.filter(Array("12", "44", "56", "78", "42", "11", "67")) should not equal (Array("12", "44", "56", "78", "42", "11", "67"))
  }

}