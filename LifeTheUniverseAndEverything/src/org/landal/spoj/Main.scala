package org.landal.spoj

object Main extends App {
	
  override def main(args: Array[String]): Unit = {  
      args.toStream.takeWhile(arg => augmentString(arg).toInt != 42).toList.foreach(println)    
  }

}