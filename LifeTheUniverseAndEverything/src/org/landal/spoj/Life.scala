package org.landal.spoj

object Life {

  implicit def string2Int(s: String): Int = augmentString(s).toInt

  def main(args: Array[String]): Unit = {

    //    for (arg <- args) {
    //      if (augmentString(arg).toInt != 42)
    //        println(arg)
    //      else return
    //    }

    filter(args).foreach(println(_))

  }

  def filter(args: Array[String]): List[String] = {
    args.toStream.takeWhile(arg => augmentString(arg).toInt != 42).toList
  }

}