package org.landal.spoj

import java.io.BufferedReader
import java.io.InputStreamReader

object LifeFromConsole {

  implicit def string2Int(s: String): Int = augmentString(s).toInt

  def main(args: Array[String]): Unit = {

    //    var ok = true
    //    while (ok) {
    //      val ln = readLine()
    //      ok = ln != null
    //      
    //      if (ok) {
    //        val input : Int = ln;
    //        if(input == 42) 
    //          return
    //          else println(input)
    //      }
    //       
    //    }

    //     val in = new BufferedReader(new InputStreamReader(System in))
    //     Stream.continually(in readLine).takeWhile(arg => augmentString(arg).toInt != 42).foreach(println(_))

    for (ln <- io.Source.stdin.getLines) {
      if (augmentString(ln).toInt == 42) return
      else println(ln)
    }

  }

}